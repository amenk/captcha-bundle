<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('rapid_data_captcha');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('sitekey')
                    ->info('Der captcha key für die Anwendung, Standard ist der Wert der env-Variable CAPTCHA_SITEKEY.')
                    ->treatNullLike('')
                    ->defaultValue('%env(resolve:CAPTCHA_SITEKEY)%')
                ->end()
                ->scalarNode('secret')
                    ->info('Das Account secret a.k.a API-Key,  Standard ist der Wert der env-Variable CAPTCHA_SECRET.')
                    ->treatNullLike('')
                    ->defaultValue('%env(resolve:CAPTCHA_SECRET)%')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
