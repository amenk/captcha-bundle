<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\EventListener;

use Contao\Form;
use Contao\FormFieldModel;
use RapidData\CaptchaBundle\Service\CaptchaInterface;

class CompileFormFieldsListener
{
    private CaptchaInterface $captcha;

    public function __construct(CaptchaInterface $captcha)
    {
        $this->captcha = $captcha;
    }

    public function __invoke(array $fields, string $formId, Form $form): array
    {
        $hasCaptcha = \count(array_filter(FormFieldModel::findPublishedByPid($form->id)->getModels(), static function (FormFieldModel $fieldModel) {
            return 'rapid_data_captcha' === $fieldModel->type;
        })) > 0;

        if ($hasCaptcha) {
            $GLOBALS['TL_HEAD'][] = $this->captcha->getStylesHtml();
            $GLOBALS['TL_HEAD'][] = $this->captcha->getScriptHtml();
        }

        return $fields;
    }
}
