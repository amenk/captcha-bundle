<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Widget;

use Contao\Controller;
use Contao\FormFieldModel;
use Contao\System;
use Contao\Widget;
use RapidData\CaptchaBundle\Service\CaptchaInterface;

/**
 * Widget wrapper around the Captcha Service for Contao Form Interop.
 */
class CaptchaWidget extends Widget
{
    /**
     * The CSS class prefix.
     *
     * @var string
     */
    protected $strPrefix = 'widget widget-recaptcha';

    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'rapid_captcha';

    private CaptchaInterface $captcha;

    public function __construct($arrAttributes = null)
    {
        parent::__construct($arrAttributes);

        $this->arrAttributes['required'] = true;
        $this->arrConfiguration['mandatory'] = true;
        $this->name = 'captcha-response';

        // @phpstan-ignore-next-line
        if (TL_MODE === 'BE') {
            return;
        }

        $this->captcha = System::getContainer()->get(CaptchaInterface::class);
    }

    public function validate(): void
    {
        // In case the value was set programmatically because of AJAX etc.
        $_POST = empty($_POST) ? Controller::getContainer()->get('request_stack')->getCurrentRequest()->request->all() : $_POST;

        if (!isset($_POST['g-recaptcha-response']) && !isset($_POST['h-captcha-response']) && !isset($_POST['captcha'])) {
            $this->class = 'error';
            $this->addError($GLOBALS['TL_LANG']['ERR']['rapid_data_captcha']['noPostVar']);

            return;
        }

        $captchaResponse = $_POST['g-recaptcha-response'];

        if (isset($_POST['h-captcha-response'])) {
            $captchaResponse = $_POST['h-captcha-response'];
        } elseif (isset($_POST['captcha'])) {
            $captchaResponse = $_POST['captcha'];
        }

        try {
            $codeValid = $this->captcha->validate($captchaResponse);

            if (!$codeValid) {
                $this->class = 'error';
                $this->addError($GLOBALS['TL_LANG']['ERR']['rapid_data_captcha']['invalid']);
            }
        } catch (\Exception $exception) {
            $logger = System::getContainer()->get('logger');
            $logger->error('[RapidCaptcha] Captcha validation failed with '.$exception->getMessage(), $exception->getTrace());

            $this->class = 'error';
            $this->addError($GLOBALS['TL_LANG']['ERR']['rapid_data_captcha']['failed']);
        }
    }

    public function generate()
    {
        // @phpstan-ignore-next-line
        if (TL_MODE === 'BE') {
            return '';
        }

        $id = 'captcha'.FormFieldModel::findById($this->id)->pid;

        return $this->captcha->getCaptchaHtml($id, $this->class, $this->getAttributes(), $this->arrConfiguration['captcha_theme'], $this->arrConfiguration['captcha_size'], $this->arrConfiguration['captcha_type']);
    }
}
