<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Service;

interface CaptchaInterface
{
    /**
     * Returns any additional styles as HTML-Nodes to display the captcha.
     */
    public function getStylesHtml(): string;

    /**
     * Returns the script tag(s) to include necessary scripts.
     */
    public function getScriptHtml(): string;

    /**
     * Returns the HTML code necessary to display the captcha.
     *
     * @param string|null $id         The optional id attribute content of the html element
     * @param string|null $class      Additional class attribute content
     * @param string      $attributes additional attributes to apply to the HTML element
     *
     * @return string the HTML to display the captcha
     */
    public function getCaptchaHtml(?string $id = null, ?string $class = null, string $attributes = '', string $theme = 'light', string $size = 'normal', string $type = 'image'): string;

    /**
     * Validates the frontend captcha code.
     *
     * @param string $data the captcha code to validate
     *
     * @return bool true if the supplied code is valid
     */
    public function validate(string $data): bool;
}
