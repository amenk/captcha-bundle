<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HCaptchaService implements CaptchaInterface
{
    private string $captchaSiteKey;

    private string $captchaSecret;

    private HttpClientInterface $httpClient;

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, HttpClientInterface $httpClient, CaptchaSettingsInterface $settingsService)
    {
        $this->captchaSiteKey = $settingsService->getSiteKey();
        $this->captchaSecret = $settingsService->getSecret();
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    public function getStylesHtml(): string
    {
        return '';
    }

    public function getScriptHtml(): string
    {
        return '<script src="https://js.hcaptcha.com/1/api.js?hl=de&render=explicit"></script>';
    }

    public function getCaptchaHtml(?string $id = null, ?string $class = null, string $attributes = '', string $theme = 'light', string $size = 'normal', string $type = 'image'): string
    {
        $attributes .= " data-theme=\"$theme\" data-size=\"$size\" data-type=\"$type\"";

        return '<div '.($id ? "id=\"$id\"" : '').' class="h-captcha'.($class ? " $class" : '').'" data-sitekey="'.$this->captchaSiteKey.'" data-callback=""'.$attributes.'></div>'
            ."<script async defer>
                window.captchas = window.captchas || [];
                setTimeout(() => {
                    if (window.captchas['${id}']) {
                        window.hcaptcha.reset(window.captchas['${id}'])
                    }
                    else {
                        window.captchas['${id}'] = window.hcaptcha.render('$id')
                    }
                }, 500);
            </script>";
    }

    /**
     * Validates a captcha response code against the hCaptcha-API.
     *
     * @param string $data The captcha response string to validate
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     *
     * @return bool whether the captcha code is valid (pass) or not (reject)
     */
    public function validate(string $data): bool
    {
        $validationPayload = [
            'secret' => $this->captchaSecret,
            'response' => $data,
        ];
        $validationPayload = new FormDataPart($validationPayload);

        $validationRequest = $this->httpClient->request(
            'POST',
            'https://hcaptcha.com/siteverify',
            [
                'headers' => $validationPayload->getPreparedHeaders()->toArray(),
                'body' => $validationPayload->bodyToIterable(),
            ]
        );

        if (200 !== $validationRequest->getStatusCode()) {
            $this->logger->error('[RapidCaptcha] Captcha verify failed: '.$validationRequest->getStatusCode());

            return false;
        }
        $validationResponse = $validationRequest->toArray();

        return  $validationResponse['success'];
    }
}
