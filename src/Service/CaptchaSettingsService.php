<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Service;

class CaptchaSettingsService implements CaptchaSettingsInterface
{
    private string $siteKey;
    private string $secret;

    public function __construct(array $extensionConfig)
    {
        $this->siteKey = $extensionConfig['sitekey'];
        $this->secret = $extensionConfig['secret'];
    }

    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }
}
